<?php

namespace Infotechnohelp\DomainSearch;
/**
 * Class State
 * @package Infotechnohelp\DomainSearch
 */
class DomainState
{
    const AVAILABLE = 1;
    const PREMIUM = 2;
    const NOT_AVAILABLE = 3;
    const FAILED = 4;

    /**
     * @var int
     */
    private $state;

    /**
     * State constructor.
     * @param int $state
     */
    public function __construct(int $state)
    {
        $this->state = $state;
    }

    /**
     * @return bool
     */
    public function isAvailable()
    {
        return $this->state === self::AVAILABLE;
    }

    /**
     * @return bool
     */
    public function isPremium()
    {
        return $this->state === self::PREMIUM;
    }

    /**
     * @return bool
     */
    public function isNotAvailable()
    {
        return $this->state === self::NOT_AVAILABLE;
    }

    /**
     * @return bool
     */
    public function isFailed()
    {
        return $this->state === self::FAILED;
    }
}