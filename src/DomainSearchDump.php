<?php

namespace Infotechnohelp\DomainSearch;

use Infotechnohelp\FileWizard\FileWizard;

/**
 * Class DomainSearchDump
 * @package Infotechnohelp\DomainSearch
 */
class DomainSearchDump
{
    /**
     * @param array $coreWords
     * @param array $endingWords
     * @param string $outputDir
     * @param bool $print
     */
    public function run(array $coreWords, array $endingWords, string $outputDir, bool $print = true)
    {
        $NameComClient = new NameComClient();

        foreach ($coreWords as $core) {

            echo ($print) ? "$core\n" : null;

            $phpDump = (new FileWizard())->createPhpDump("$outputDir/$core")->write([
                /*
                'available' => [],
                'premium' => [],
                'not available' => [],
                'failed' => [],
                */
            ]);


            foreach ($endingWords as $ending) {
                echo ($print) ? "." : null;

                $domain = "$core$ending.com";

                if ($NameComClient->getState($domain)->isAvailable()) {
                    $phpDump->push($domain, 'available');
                    echo ($print) ? "\n$domain Available\n" : null;
                    continue;
                }

                if ($NameComClient->getState($domain)->isPremium()) {
                    $phpDump->push($domain, 'premium');
                    continue;
                }

                if ($NameComClient->getState($domain)->isNotAvailable()) {
                    $phpDump->push($domain, 'not available');
                    continue;
                }

                $phpDump->push($domain, 'failed');
            }

            echo ($print) ? "\n\n" : null;
        }
    }
}