<?php

namespace Infotechnohelp\DomainSearch;

use Infotechnohelp\PhantomPhp\Client;

/**
 * Class NameComClient
 * @package Infotechnohelp\DomainSearch
 */
class NameComClient extends Client
{
    /**
     * @var string
     */
    private $url = 'https://www.name.com/domain/search/';

    /**
     * @param string $domain
     * @return string|null
     */
    private function getResponse(string $domain): ?string
    {
        $url = "{$this->url}$domain";

        $validationCallable = function ($html) {
            $this->html->load($html);

            $result = $this->html->find('div#exactMatch', 0)->children(0)->children(0)->plaintext;

            if ($result !== 'Searching') {
                return true;
            }

            return false;
        };

        return $this->getValidResponse($url, $validationCallable);
    }

    /**
     * @param string $domain
     * @return DomainState
     */
    public function getState(string $domain): DomainState
    {
        $response = $this->getResponse($domain);

        if (!$response) {
            return new DomainState(DomainState::FAILED);
        }

        $this->html->load($response);

        if ($this->html->find('div#exactMatch', 0)->find('center', 0) === null) {

            $message = $this->html->find('div#exactMatch', 0)->find('p', 0)->innertext;

            if (strpos($message, 'This is a premium domain!') === false) {
                return new DomainState(DomainState::AVAILABLE);
            } else {
                return new DomainState(DomainState::PREMIUM);
            }

        }

        return new DomainState(DomainState::NOT_AVAILABLE);
    }
}